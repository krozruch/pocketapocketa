<!doctype=html>
<html lang="en">
<head>
    <title>Neurodiversity</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <link type="text/css" rel="stylesheet" href="../samizdat-double.css"/>
</head>
<body>
<header>
</header>

<h1>Neurodiversity</h1>

<p>
  Neurodiversity is a movement underpinned by the idea that there are all kinds of brains and that the civilisation we know could not have developed without a diversity of neural approaches, that is, a variety of temperaments and styles of thinking. There are, inevitably and indeed appropriately, a variety of conceptions of neurodiversity. My own, honed through years of experience of neurodevelopmental conditions such as ADHD, Asperger's syndrome, and indeed depression and cyclothymia, does not seek to downplay the difficulties of these conditions, exaggerate the benefits, nor to claim that there be no associated disbenefits at all in a world more fairly ordered. Instead, I tend to acknowledge the challenges of some of these conditions at the same time as examining the very real benefits to society as well as the individual they may bring. What this means in practise, especially given my many years of experience of working in a range of educational environments, is that I am keen to research and explore all of the ways that people may learn to use their brains and their temperaments to benefit themselves and others.
</p>

<p>
  On the surface of things, depression is one of the conditions mentioned above that gives an individual far less than it takes away. Depressive lucidity, the phenomenon where people who are depressed appear to see the world in more objective terms than their "euthymic"<sup>1</sup> peers, is one example where the experience of having been depressed, if not the state of depression itself, might give authors, philosophers, politicians, artists, and others a perspective absent in others. Depression is common enough in notable persons that any lay reader used to reading biographies of people who have achieved renown in any number of fields must surely have come across it. Even if this were not the case, there would be so many possibilities to choose from that compiling a whistlestop tour of famous depressives would be as arbitrary an activity as any magazine list of top ten films or albums of a given decade. Whatever the fact of this, many sufferers of depression (and the term sufferer, while it may be controversial below, is uncontroversial here) are themselves irritated by the notion that there may be anything positive or ennobling about depression and if one may from time to time come upon people who think that "curing" or ameliorating the condition might rob them of the source of their creativity, those who hold such views who have themselves demonstrably suffered from depression are the exceptions that prove the rule.
</p>

<p>
  On the other extreme there is autism and asperger's syndrome. For reasons we might be able to analyse, many of those who suffer from asperger's syndrome<sup>2</sup>, as opposed to autism, react angrily to any notion that the symptoms of asperger's syndrome may be improved, still less "cured". In one sense, they are reacting to the often vocal parents of those with autism who may be found in certain communities to discuss the difficulties of looking after their sons and daughters with little thought to their feelings or respect for their privacy. Additionally, since people with asperger's syndrome regularly experience hostility in most every social circle they encounter, it may be natural that they are assertive to the point of aggressive in talking up the benefits of asperger's syndrome. These benefits are real. An often quoted passage from Hans Asperger, has it that scientists need a little autism, and it is axiomatic that people working in technical fields, mathematics, science, and computer science are often "on the [autistic] spectrum". The same quote in fact throws in artists. Though this may be more seldom acknowledged, I am convinced that it is not merely writers of speculative fiction who may disproportionately show signs of asperger's syndrome and that, indeed, the sheer bloodymindedness necessary for those from less privileged social groups to attain any kind of renown and success in any creative field, means that many of us trying to do so are in fact aspergic.
</p>

<p>
  On to ADHD. Here the scale of benefits to disbenefits may have been comprehensively tipped in favour of disbenefits by the industrialisation of society. Not only is ADHD, like the above conditions, made worse by the industrialisation of food production, the conditions in schools and the frankly God awful<sup>3</sup> jobs people are expected to perform, especially when they do not achieve in school by sitting down on their asses and performing pointless academic tasks year upon year, provide few options for people who might once have lived happy lives as craftspeople. Where we are now, people with ADHD would rarely make the claim that the benefits outweigh the disbenefits and most attempt to do something to improve their condition. Nevertheless, people with ADHD typically have a great number of ideas and a great deal of energy. They can also be very perseverant, pushing on even after any number of painful defeats.
</p>

<p>
  Finally, to myself. I have had all of these conditions. If I were to be flippant about it, I might say that my asperger's syndrome is the reason I am writing this as raw html into emacs running on the command line of a Linux virtual machine running on a hypervisor, that my ADHD is the reason I am writing this instead of and as well as my novel, and the experiences I have had with depression are the reason my novel is so pessimistic about the direction we are going in with the technologies of surveillance capitalism having corroded democracy utterly over the last ten years. In fact, all of this would break down. People with ADHD can often hyperfocus, and the skills of brainstorming and the moreish addiction to puzzle solving might well map to the strengths of ADHD are, I would argue, as good a fit for a certain style of creative engagement with technology as the obsessive analytical logical styles of thinking of asperger's syndrome. Either style of thinking could adapt well to the kinds of difficulty I will encounter if I continue to develop this website as a Flask or Django web app. Depression, and indeed manic depressive illness, is a trickier one to place here. Hypomania, like ADHD, can be associated with creativity and an incredible drive. Depression, and in particular cyclical depression, might lead one to over and over again search for resilient meaning in a project or activity; if nothing else, the domestic criticism, as the cyclothymic Keats had it, of one with a cyclical form of depression, can lead to something like the penetration testing of a creative project, making it, as Hemingway had it, stronger in all the broken parts. This, pretty much, is how Kay Redfield Jamison, manic depressive author of Touched With Fire, had it when she wrote about the relation between depression and creativity in a book which, as the late, downbeat author, Jenny Diski, once put it to me, was the worst of the lot of a glut of books romanticising the curse.
</p>

<p>
  Ultimately, however clear it may be that I am Aspergic and that I have ADHD (depression is something I have been able for the most part to keep on top of)<sup>4</sup>, it would to my mind be a fool who attempted to state with any claim to authority which of these conditions is responsible for so complex and multifactoral a behaviour as writing a novel, a fondness for coding, or a preference for an obscure operating system.
</p>

<p>
  Such fools exist and will crop up in the life of anybody who has known neurodevelopmental conditions whether they may reasonably be said to suffer from them or not. Ultimately, what we have to remember in order to keep our patience is that the neurotypical mind was not designed to be good at thinking. The mind was not designed at all. It is a beautiful, idiosyncratic tool whose beauty and idiosyncracy vary from one exemplar to the next. Those neurotypicals who are given to think (and I sometimes conclude that there are few of them who are in the habit of thinking regularly without some external motivator), might, if they were honest with themselves, come close to admitting that they, and indeed the societies we live in, "suffer" no less for the workings of their mind than we do.
</p>

<p>
  And there it is. A majority reading this (if it be evenly distributed throughout the population), will know what it is to be judged for the operation of their own mind. My own mind, for what it's worth, has led me in all manner of directions. There would be much to regret if that would be of use to anybody. Instead, I have tended to explore it, and I will do so again sometime. Now, it is not the focus. <a href="mailto:krozruch@pocketapocketa.cz?subject=Neurodiversity - index">But let me know</a> if that is something you would like to see.
</p>

<br /><br />


<p class="footnote"><sup>1</sup> It is common in texts discussing neurodiversity to use terms that may be unfamiliar for states and conditions that may be considered to be "default". Euthymia describes a mood state thought to be in balance. In fact, once examined, it is no more neutral than any other if one is to consider in terms of intellectual objectivity. The scales are turned, as described in this paragraph, towards the positive.</p>

<p class="footnote"><sup>2</sup> The term Asperger's syndrome was dropped in the DSM V. I use it because it is, to me, one of the most meaningful and precise terms used in previous editions of the Diagnostic and Statistical Manual of Mental Disorders, resistant, I would argue, to any effort of falsification you throw at it, and because the DSM was not and will not be my bible for any number of reasons. I regret that I am not always proud to call myself aspergic, but that I know what it means, I relate to it, and it explains a clutch of aspects of my personality, my behaviour, and my thinking better than anything else. This is also, in any case, true of the original insights of the avuncular Hans Asperger who may be less convenient to any number of psychiatrists and psychopharmacologists than others who have examined autism, but outranks every one. I have never come across anybody who plausibly identifies as autistic who has a problem with the term. If the term may not necessarily outlive the DSM, it may outlive its usefulness, if it has indeed not done so already.</p>

<p class="footnote"><sup>3</sup> File under depressive lucidity if you must, but keep your eyes peeled the coming month and see if you will be able to disagree that most of the jobs you come across are just downright unpleasant from start to finish. Me, I would sooner be swinging a hammer at an anvil or building a house out of wood, perhaps, but most of the jobs I have been offered and suffered in my life have been a torture to me, and if that is down to my temperament, then still, many people in many towns face a objectively unenviable task in our days of deskilling, and I won't be convinced that most people in open offices have it any better. Sorry.</p>

<p class="footnote"><sup>4</sup> For the most part, the worst episodes of my depression preceeded my diagnosis with ADHD "with traits of autism", and the subsequent consistency with which I was able to implement a diet which significantly ameliorated all of the conditions described above. Depression was the first to lift though it doesn't ever pay to be complacent.</p>

<footer>
  <p class="center">
    <a href="../index.html">home</a>
  </p>
</footer>
</html>
</body>
